export class Router {
  routes: Array<{re: string, handler: () => void, state: {}}>;
  _root: string;
  _currentRoute: string;
  interval: number;
  constructor() {
    this.routes = [];
    this._root = '/';
    this._currentRoute = window.location.pathname;
  }

  add(re: string, handler: () => void, state: {}) {
    const routeExists: boolean = Boolean(this.routes.find(r => r.re === re));
    if (!routeExists) this.routes.push({ re, handler, state });
    return this;
  }

  check(f: string) {
    const fragment = f || this.getFragment();
    for (let i = 0; i < this.routes.length; i++) {
      const match = fragment.match(this.routes[i].re);
      if (match) {
        match.shift();
        this.routes[i].handler.apply({}, match);
        return this;
      }
    }
    return this;
  }

  clearSlashes(path: string) {
    return path.toString().replace(/\/$/, '').replace(/^\//, '');
  }

  currentHandler() {
    const route = this.routes.find(r => r.re === this.currentRoute);
    return route;
  }

  getFragment() {
    let fragment = this.clearSlashes(decodeURI(`${location.pathname}${location.search}`));
    fragment = fragment.replace(/\?(.*)$/, '');
    fragment = this.root !== '/' ? fragment.replace(this.root, '') : fragment;
    return this.clearSlashes(fragment);
  }

  linkHandler(ev: {detail: {route: string}}) {
    this.navigate(ev.detail.route);
  }

  listen() {
    let current = this.getFragment();
    const fn = () => {
      if (current !== this.getFragment()) {
        current = this.getFragment();
        this.check(current);
      }
    };
    clearInterval(this.interval);
    this.interval = setInterval(fn, 100);
    return this;
  }

  navigate(path: string) {
    const _path = path ? path : '';
    this.currentRoute = `${this.root}${this.clearSlashes(_path)}`;
    history.pushState(null, null, this.currentRoute);
    this.routes.forEach(r => r.handler.call(this));
    // this.runCurrentHandler();
    return this;
  }

  remove(param: string) {
    const rIdx = this.routes.findIndex(r => r.re.toString() === param.toString());
    if (rIdx > -1) this.routes.splice(rIdx, 1);
    return this;
  }

  reset() {
    this.routes = [];
    this.root = '/';
    this._currentRoute = this.root;
    return this;
  }

  runCurrentHandler() {
    const route = this.currentHandler();
    if (route) route.handler.call(this);
  }

  get currentRoute() {
    return this._currentRoute;
  }

  set currentRoute(value) {
    this._currentRoute = value;
  }

  get root() {
    return this._root;
  }

  set root(root) {
    this._root = root;
  }
}
