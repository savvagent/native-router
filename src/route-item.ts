import { Router } from './Router';

class RouteItem extends HTMLElement {
  component: { name: string, state: {} };
  path: string;
  _router: Router;
  constructor() {
    super();
  }

  attributeChangedCallback(name: string, oVal: string, nVal: string) {
    if (name === 'path') this.path = nVal;
    else if (name === 'compontent') {
      const { name, state } = JSON.parse(nVal);
      this.component = { name, state };
    }
  }

  routeHandler() {
    if (/^</.test(this.component.name)) {

    } else {
      const component = 
    }
  }

  get router() {
    return this._router;
  }

  set router(router) {
    this._router = router;
    if (this.path && this.component && this.component.state) {
      this._router.add(this.path, this.routeHandler, this.component.state);
    }
    console.log(`this._router`, this._router)
  }

  static get observedAttributes(): string[] {
    return [ 'component', 'path' ];
  }
}
customElements.get('route-item') || customElements.define('route-item', RouteItem);

export { RouteItem };
