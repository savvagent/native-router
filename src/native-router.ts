import { Router } from './Router';

class NativeRouter extends HTMLElement {
  _router: Router;
  interval: number;
  constructor() {
    super();
    this.interval = 50;
  }

  connectedCallback() {
    const children = this.childNodes;
    const id = setInterval(() => {
      const routeItems = [...children].filter(c => c.localName === 'route-item');
      if (this._router && routeItems.length) {
        routeItems.forEach(ri => ri.router = this._router);
        clearInterval(id);
      }
    }, this.interval)
  }

  get router() {
    return this._router;
  }

  set router(router) {
    this._router = router;
  }

}
customElements.get('native-router') || customElements.define('native-router', NativeRouter);

export { NativeRouter };
