class LinkItem extends HTMLElement {
  clickListener: () => void;
  link: HTMLElement;
  route: string;
  text: string;
  constructor() {
    super();
    this.attachShadow({mode: 'open'});
    this.clickListener = this.clickHandler.bind(this);
  }

  attributeChangedCallback(name: string, oVal: string, nVal: string) {
    this[name] = nVal;
  }

  clickHandler(ev: { preventDefault: () => void, detail: {route: string}}) {
    ev.preventDefault();
    document.dispatchEvent(new CustomEvent('link-item:click', { detail: { route: this.route }}));
  }

  connectedCallback() {
    this.shadowRoot.innerHTML = `<a href=${this.route}>${this.text}</a>`;
    this.link = this.shadowRoot.querySelector('a');
    this.link.addEventListener('click', this.clickListener)
  }

  disconnectedCallback() {
    this.link.removeEventListener('click', this.clickListener);
  }

  static get observedAttributes(): string[] {
    return [ 'route', 'text' ];
  }
}
customElements.get('link-item')  || customElements.define('link-item', LinkItem);

export { LinkItem };
