import de from './component_de.json';
import en from './component_en.json';
import eo from './component_eo.json';
import es from './component_es.json';
import fr from './component_fr.json';
import it from './component_it.json';
import ja from './component_ja.json';
import ko from './component_ko.json';
import nl from './component_nl.json';
import pl from './component_pl.json';
import pt from './component_pt.json';
import ru from './component_ru.json';
import sv from './component_sv.json';
import zh from './component_zh.json';

export {
  de,
  en,
  eo,
  es,
  fr,
  it,
  ja,
  ko,
  nl,
  pl,
  pt,
  ru,
  sv,
  zh
};
