const root = process.cwd();
import { resolve } from 'path';
import liveServer from 'rollup-plugin-live-server';
import { terser } from 'rollup-plugin-terser';
import typescript from 'rollup-plugin-typescript';

const input = resolve(__dirname, 'src', 'index.ts');

let tasks = [];

const devTasks = [
  {
    input,
    plugins: [
      typescript(),
      liveServer({
        file: 'index.html',
        spa: true,
        port: 3100,
        host: '0.0.0.0',
        root: './demo',
        mount: [
          [ '/demo', './demo' ],
          [ '/dist', './dist' ],
          [ '/node_modules', './node_modules' ],
          [ '/src', './src' ]
        ],
        open: false,
        wait: 500
      })
    ],
    output: {
      file: resolve(root, 'dist', 'native-router.js'),
      format: 'es'
    },
    watch: {
      chokidar: true,
      clearScreen: true,
      include: resolve(root, 'src', '**')
    }
  }
];

const prodTasks = [
  {
    input,
    plugins: [
      typescript(),
      terser()
    ],
    output: {
      file: resolve(root, 'dist', 'native-router.min.js'),
      format: 'es'
    }
  }
];

if (process.argv.some(key => key === '-cw' || key === '-wc')) tasks = [ ...tasks, ...devTasks ];
else tasks = [ ...tasks, ...prodTasks ];

export default tasks;
